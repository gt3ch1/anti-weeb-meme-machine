package me.gt3ch1.sql;

import java.sql.*;

import me.gpease.ml.arrays.ImageArray;
import me.gpease.ml.arrays.UserArray;
import me.gpease.ml.math.Pair;

/**
 *
 * @author Gavin
 */
public class SqlFunctions {

	public void CreateConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
			// Connect up to an SQL server
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.3.1:3306/ImageML", "root", "hRNmm2OVt1bZ8EUf");
			Statement stmt = con.createStatement();
			// Reset ImageArray
			ImageArray.clearAll();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Image");
			while (rs.next()) {
				// Put in all of the goodies into the arrays in ImageArray
				ImageArray.allWidths.add(new Pair(rs.getDouble("width"), rs.getBoolean("isWeeb")));
				ImageArray.allHeights.add(new Pair(rs.getDouble("height"), rs.getBoolean("isWeeb")));
				ImageArray.allBrightness.add(new Pair(Math.abs(rs.getDouble("brightness")), rs.getBoolean("isWeeb")));
				ImageArray.allReds.add(new Pair(rs.getDouble("reds"), rs.getBoolean("isWeeb")));
				ImageArray.allBlues.add(new Pair(rs.getDouble("blues"), rs.getBoolean("isWeeb")));
				ImageArray.allGreens.add(new Pair(rs.getDouble("greens"), rs.getBoolean("isWeeb")));
				ImageArray.allConfidence.add(new Pair(rs.getDouble("confidence"), rs.getBoolean("isWeeb")));
			}
			rs = stmt.executeQuery("SELECT DISTINCT User, UserWeight FROM Image");
			while (rs.next()) {
				UserArray ua = new UserArray();
				ua.addUser(rs.getString("User"), rs.getDouble("UserWeight"));
			}
			// Close the connection
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void Upload(int width, int height, int abs, int reds, int blues, int greens, double confidence, boolean doCalculation) {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection("jdbc:mysql://192.168.3.1:3306/ImageML", "root",
					"hRNmm2OVt1bZ8EUf");
			Statement stmt = con.createStatement();
			stmt.execute("INSERT INTO `Image` (`width`, `height`, `brightness`, `reds`, `greens`, `blues`, `confidence`, `isWeeb`)"
					+ " VALUES ( "+width+","+height+","+abs+","+reds+","+greens+","+blues+","+confidence/100+","+doCalculation+")");
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
