package me.gpease.ml.image;

import java.util.ArrayList;

public class Image {
	private String name;
	private int width;
	private int height;
	private double brightness;
	private int reds;
	private int blues;
	private int greens;
	private boolean isWeeb;
	private Picture p;

	/**
	 * Creates an Image object with the parameters given.
	 * 
	 * @param p
	 * @param name
	 * @param width
	 * @param height
	 * @param brightness
	 * @param reds
	 * @param blues
	 * @param greens
	 * @param isWeeb
	 */
	public Image(Picture p, String name, int width, int height, double brightness, int reds, int blues, int greens,
			boolean isWeeb) {
		this.name = name;
		this.width = width;
		this.height = height;
		this.brightness = brightness;
		this.reds = reds;
		this.blues = blues;
		this.greens = greens;
		this.isWeeb = isWeeb;
		this.p = p;
	}

	public String toString() {
		return "{" + name + ", " + width + ", " + height + ", " + brightness + ", " + reds + ", " + blues
				+ ", " + greens + ", " + isWeeb +"}";
	}

	/**
	 * @return Picture p
	 */
	public Picture getPicture() {
		return p;
	}

	/**
	 * @return if the weeb flag is sat
	 */
	public boolean getWeeb() {
		return isWeeb;
	}

	/**
	 * Sets the weeb flag for the Image
	 * @param b
	 */
	public void setWeeb(boolean b) {
		this.isWeeb = b;
	}

}
