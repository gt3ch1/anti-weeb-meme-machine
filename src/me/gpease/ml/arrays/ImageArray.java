package me.gpease.ml.arrays;

import java.util.ArrayList;
import java.util.Collections;

import me.gpease.ml.math.Pair;

public class ImageArray {
	/*
	 * Create all of the ArrayLists for the rows in SQL
	 */
	public static ArrayList<Pair> allWidths = new ArrayList<>();
	public static ArrayList<Pair> allHeights = new ArrayList<>();
	public static ArrayList<Pair> allBrightness = new ArrayList<>();
	public static ArrayList<Pair> allReds = new ArrayList<>();
	public static ArrayList<Pair> allBlues = new ArrayList<>();
	public static ArrayList<Pair> allGreens = new ArrayList<>();
	public static ArrayList<Pair> allConfidence = new ArrayList<>();
	public static double isWeebCounter = 0;
	public static double confidence = 0.0;
	
	/**
	 * Calculates whether or not the properties given are identical to those that are of a anime meme.
	 * @param width
	 * @param height
	 * @param brightness
	 * @param reds
	 * @param blues
	 * @param greens
	 * @param knn
	 * @return
	 * true if it is deemed by the given parameters that the image is a weeb meme, false if otherwise.
	 */
	public static boolean Classify(double width, double height, double brightness, double reds, double blues,
			double greens, int knn) {

		// WIDTH
		for (int i = 0; i < allWidths.size(); i++) {
			double distance = allWidths.get(i).distance;
			double newDistance = Math.abs(Math.pow(distance, 2) - Math.pow(width, 2));
			allWidths.get(i).distance = Math.sqrt(newDistance);

		}
		// HEIGHT
		for (int i = 0; i < allHeights.size(); i++) {
			double distance = allHeights.get(i).distance;
			double newDistance = Math.abs(Math.pow(distance, 2) - Math.pow(height, 2));
			allHeights.get(i).distance = Math.sqrt(newDistance);
		}
		// BRIGHTNESS
		for (int i = 0; i < allBrightness.size(); i++) {
			double distance = Math.abs(allBrightness.get(i).distance);
			double newDistance = Math.abs(Math.pow(distance, 2) - Math.pow(brightness, 2));
			allBrightness.get(i).distance = Math.sqrt(newDistance);
		}
		// REDS
		for (int i = 0; i < allReds.size(); i++) {
			double distance = allReds.get(i).distance;
			double newDistance = Math.abs(Math.pow(distance, 2) - Math.pow(reds, 2));
			allReds.get(i).distance = Math.sqrt(newDistance);
		}
		// BLUES
		for (int i = 0; i < allBlues.size(); i++) {
			double distance = allBlues.get(i).distance;
			double newDistance = Math.abs(Math.pow(distance, 2) - Math.pow(blues, 2));
			allBlues.get(i).distance = Math.sqrt(newDistance);
		}
		// GREENS
		for (int i = 0; i < allGreens.size(); i++) {
			double distance = allGreens.get(i).distance;
			double newDistance = Math.abs(Math.pow(distance, 2) - Math.pow(greens, 2));
			allGreens.get(i).distance = Math.sqrt(newDistance);
		}
		// CONFIDENCE
		for (int i = 0; i < allConfidence.size(); i++) {
			confidence += allConfidence.get(i).distance;
		}
		
		// Sorting time!
		Collections.sort(allWidths);
		Collections.sort(allHeights);
		Collections.sort(allBrightness);
		Collections.sort(allReds);
		Collections.sort(allBlues);
		Collections.sort(allGreens);
		/*
		 * Get knn elements in the array
		 */
		for (int i = allWidths.size() - 1; i > knn - 1; i--) {
			allWidths.remove(i);
		}
		for (int i = allHeights.size() - 1; i > knn - 1; i--) {
			allHeights.remove(i);
		}
		for (int i = allBrightness.size() - 1; i > knn - 1; i--) {
			allBrightness.remove(i);
		}
		for (int i = allReds.size() - 1; i > knn - 1; i--) {
			allReds.remove(i);
		}
		for (int i = allBlues.size() - 1; i > knn - 1; i--) {
			allBlues.remove(i);
		}
		for (int i = allGreens.size() - 1; i > knn - 1; i--) {
			allGreens.remove(i);
		}
		/*
		 * Count how much weeb elements there are in the 
		 * knn elements
		 */
		// WIDTH
		for (int i = 0; i < allWidths.size(); i++) {
			if (allWidths.get(i).getBoolean())
				isWeebCounter++;
		}
		// HEIGHT
		for (int i = 0; i < allHeights.size(); i++) {
			if (allHeights.get(i).getBoolean())
				isWeebCounter++;
		}
		// BRIGHTNESS
		for (int i = 0; i < allBrightness.size(); i++) {
			if (allBrightness.get(i).getBoolean())
				isWeebCounter++;
		}
		// REDS
		for (int i = 0; i < allReds.size(); i++) {
			if (allReds.get(i).getBoolean())
				isWeebCounter++;
		}
		// BLUES
		for (int i = 0; i < allBlues.size(); i++) {
			if (allBlues.get(i).getBoolean())
				isWeebCounter++;
		}
		// GREENS
		for (int i = 0; i < allGreens.size(); i++) {
			if (allGreens.get(i).getBoolean())
				isWeebCounter++;
		}
		
		isWeebCounter = isWeebCounter/confidence;
		isWeebCounter = isWeebCounter+(confidence*2);
		
		System.out.println("WEEB COUNTER: " + isWeebCounter + "\n NUMBER NEEDED: " + (((knn)*confidence / 2) )*confidence);
		if (isWeebCounter > ((knn*(confidence / 2))*confidence)) {
			return true;
		}
		return false;
	}
	/**
	 * Resets the arrays and other items used in Classify
	 */
	public static void clearAll() {
		allWidths.clear();
		allHeights.clear();
		allBrightness.clear();
		allReds.clear();
		allBlues.clear();
		allGreens.clear();
		allConfidence.clear();
		isWeebCounter = 0;
		confidence = 0;
	}
}
