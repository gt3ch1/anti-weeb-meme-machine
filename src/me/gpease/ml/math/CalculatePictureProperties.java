package me.gpease.ml.math;

import me.gpease.ml.image.Image;

public class CalculatePictureProperties {
	Image img;

	/**
	 * 
	 * @param Image i
	 */
	public CalculatePictureProperties(Image i) {
		this.img = i;
	}

	public int getWidth() {
		return img.getPicture().width();
	}

	public int getHeight() {
		return img.getPicture().height();
	}

	/**
	 * Calculates the average brightness of the image
	 * @return
	 */
	public int calculateBrightness() {
		int brightness = 0;
		for (int x = 0; x < img.getPicture().width(); x++) {
			for (int y = 0; y < img.getPicture().height(); y++) {
				brightness += img.getPicture().get(x, y).getRed();
				brightness += img.getPicture().get(x, y).getGreen();
				brightness += img.getPicture().get(x, y).getBlue();
			}
		}
		return brightness / (img.getPicture().width() * img.getPicture().height());
	}

	/**
	 * Calculates the average amount of red in the image.
	 * @return
	 */
	public int calculateReds() {
		int reds = 0;
		for (int x = 0; x < img.getPicture().width(); x++) {
			for (int y = 0; y < img.getPicture().height(); y++) {
				reds += img.getPicture().get(x, y).getRed();
			}
		}
		return reds;
	}

	/**
	 * Calculates the average amount of blue in the image.
	 * @return
	 */
	public int calculateBlue() {
		int blues = 0;
		for (int x = 0; x < img.getPicture().width(); x++) {
			for (int y = 0; y < img.getPicture().height(); y++) {
				blues += img.getPicture().get(x, y).getBlue();
			}
		}
		return blues;
	}

	/**
	 * Calculates the average amount of green in the image.
	 * @return
	 */
	public int calculateGreen() {
		int greens = 0;
		for (int x = 0; x < img.getPicture().width(); x++) {
			for (int y = 0; y < img.getPicture().height(); y++) {
				greens += img.getPicture().get(x, y).getGreen();
			}
		}
		return greens;
	}
}
