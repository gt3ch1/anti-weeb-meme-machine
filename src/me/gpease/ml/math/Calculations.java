package me.gpease.ml.math;

import java.io.File;

import me.gpease.ml.arrays.ImageArray;
import me.gpease.ml.image.Picture;
import me.gt3ch1.sql.SqlFunctions;

public class Calculations {
	public static double c;
	/**
	 * f is the location of an image that wil be scored, and then
	 * have the data about that image uploaded to an SQL database.
	 * @param f
	 */
	public static void doUpload(File f) {
		Picture p = new Picture(f);
		int brightness = 0;
		int reds = 0;
		int blues = 0;
		int greens = 0;
		int area = p.width() * p.height();
		for (int x = 0; x < p.width(); x++) {
			for (int y = 0; y < p.height(); y++) {
				reds += p.get(x, y).getRed();
				blues += p.get(x, y).getBlue();
				greens += p.get(x, y).getGreen();
				brightness += reds + blues + greens;
			}
		}
		brightness = brightness / area;
		reds = reds / area;
		blues = blues / area;
		greens = greens / area;
		// Run the calculations
		boolean isWeeb = doCalculation(f);
		// Upload the information about the image
		new SqlFunctions().Upload(p.width(), p.height(), Math.abs(brightness), reds, blues, greens, getConfidence(),
				isWeeb);

	}

	/**
	 * Runs a few calculations on f, and decides if f is a garbage meme or not.
	 * @param f
	 * @return
	 * true if f is dubbed to be a weeb meme, false if otherwise.
	 */
	public static boolean doCalculation(File f) {
		ImageArray.clearAll();
		Picture p = new Picture(f);
		// Create SQL connections
		new SqlFunctions().CreateConnection();
		int brightness = 0;
		int reds = 0;
		int blues = 0;
		int greens = 0;
		int area = p.width() * p.height();
		for (int x = 0; x < p.width(); x++) {
			for (int y = 0; y < p.height(); y++) {
				reds += p.get(x, y).getRed();
				blues += p.get(x, y).getBlue();
				greens += p.get(x, y).getGreen();
				brightness += reds + blues + greens;
			}
		}

		brightness = Math.abs(brightness) / area;
		reds = reds / area;
		blues = blues / area;
		greens = greens / area;
		/*
		 * Call to ImageArray.Classify
		 * 3 is currently the k nearest neighbors.
		 */
		ImageArray.Classify(p.width(), p.height(), Math.abs(brightness), reds, blues, greens, 3);
		/* 
		 * "Confidence" is how likely the image is a weeaboo meme. 
		 */
		double confidence = ((double) ImageArray.isWeebCounter / ImageArray.allWidths.size() * ImageArray.confidence);
		setConfidence(confidence);
		/*
		 * "ImageArray" confidence is the logarithmic confidence based off of what all
		 * of the other knn images are, multiplied by how much weeb-detectors
		 */
		System.out.println("ImageArray confidence: " + Math.log(ImageArray.confidence * ImageArray.isWeebCounter));
		/*
		 * "ML" confidence is the logarithmic confidence of "Confidence" multiplied by weeb-counter, divided by two.
		 * 
		 */
		System.out.println("ML Confidence: " + Math.log(confidence * ImageArray.isWeebCounter) / 2);
		// This is where "ML" confidence is used.  1.89 is generally where an image becomes a garbage meme.
		if ((Math.log(confidence * ImageArray.isWeebCounter) / 2 > 1.89325794)) {
			System.out.print("File " + f.toString() + " is {WEEB}, CONFIDENCE: " + confidence);
			return true;
		} else {
			System.out.println("File " + f.toString() + " is {NOT WEEB}, CONFIDENCE: " + confidence * 1);
			return false;
		}

	}

	public static double getConfidence() {
		return c;
	}

	public static void setConfidence(double confidence) {
		c = confidence;
	}

}
