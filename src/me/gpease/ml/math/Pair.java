package me.gpease.ml.math;

public class Pair implements Comparable<Pair>{
	public double distance= 0;
	private boolean isWeeb = false;
	
	/**
	 * Creates a new Pair item
	 * @param w
	 * @param b
	 */
	public Pair(double w, boolean b) {
		distance = w;
		isWeeb= b;
	}
	
	/**
	 * @return distance
	 */
	public double getDistance() {
		return distance;
	}
	/**
	 * @return if the property is true
	 */
	public boolean getBoolean() {
		return isWeeb;
	}
	/**
	 * Compare self to Pair o
	 */
	@Override
	public int compareTo(Pair o) {
		if(distance > o.getDistance())
			return 1;
		else if(distance == o.getDistance())
			return 0;
		else
			return -1;
	}
	@Override
	public String toString() {
		return "{" + distance + ", " + isWeeb + "}";
	}
}
