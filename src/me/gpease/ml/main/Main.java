package me.gpease.ml.main;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import me.gpease.ml.arrays.ImageArray;
import me.gpease.ml.image.Picture;
import me.gpease.ml.math.Calculations;
import me.gt3ch1.sql.SqlFunctions;

public class Main {
	// f is the reference to the File we will be testing on
	static File f = null;
	// c is for the confidence scoring, used in doUpload()
	public static double c;
	
	/**
	 * main takes in two arguments, one is a command (calculate or upload), and the other is a file.
	 * @param args
	 */
	public static void main(String[] args) {
		// If there are two arguments
		if (args.length == 2) {
			// If the first argument is calculate
			if (args[0].equalsIgnoreCase("/calculate")) {
				try {
					// the file is the second argument
					f = new File(args[1]);
					// see if the file is a weeb meme
					Calculations.doCalculation(f);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// If the second argument is upload
			if (args[0].equalsIgnoreCase("/upload")) {
				try {
					// the file is the second argument
					f = new File(args[1]);
					// see if the file is a weeb meme, and upload the data
					Calculations.doUpload(f);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		/*
		 * This block is primarily used when there are no arguments given
		 * to the program.
		 */
		} else {
			String current = null;
			try {
				current = new java.io.File(".").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Current dir:" + current);
			Calculations.doCalculation(new File("src\\me\\gpease\\img\\notanime2.jpg"));
		}

	}

	}
