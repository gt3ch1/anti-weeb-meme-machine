package me.gpease.testing;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import me.gpease.ml.main.Main;
import me.gpease.ml.math.Calculations;

public class Testing {

	/**
	 * Tests to see if the file 'notanime' is not an anime image.
	 */
	@Test
	public void testNotAnime1() {
		File f = new File("src/me/gpease/img/notanime.jpg");
		boolean isWeeb = Calculations.doCalculation(f);
		assertEquals("Testing to see if notanime.jpg returns as not weeb.", false, isWeeb);
	
	}
	/**
	 * Tests to see if the file 'notanime2' is not an anime image.
	 */
	@Test
	public void testNotAnime2() {
		File f = new File("src/me/gpease/img/notanime2.jpg");
		boolean isWeeb = Calculations.doCalculation(f);
		assertEquals("Testing to see if notanime2.jpg returns as not weeb.", false, isWeeb);
	
	}
	/**
	 * Tests to see if the file 'anime1' is an anime image.
	 */
	@Test
	public void testAnime1() {
		File f = new File("src/me/gpease/img/anime1.jpg");
		boolean isWeeb = Calculations.doCalculation(f);
		assertEquals("Testing to see if anime1.jpg returns as weeb.", true, isWeeb);
	
	}
	/**
	 * Tests to see if the file 'anime2' is an anime image.
	 */
	@Test
	public void testAnime2() {
		File f = new File("src/me/gpease/img/anime2.jpg");
		boolean isWeeb = Calculations.doCalculation(f);
		assertEquals("Testing to see if anime2.jpg returns as weeb.", true, isWeeb);
	
	}

}
